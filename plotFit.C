// Three component exponential decay convoluted with a gaussian
// A gives intermediate fraction
double conv_3comp_exp(double* x, double* params)
{
  double tau1 = params[0];
  double tau2 = params[1];
  double p = params[2];
  double sigma = params[3];
  double amp = params[4];
  double t_0 = params[5];
  double tau3 = params[6];
  double A = params[7];
  double t = x[0] - t_0;

  long double arg1 = (-t/tau1 + sigma*sigma/(2*tau1*tau1));
  long double arg2 = (-t/tau2 + sigma*sigma/(2*tau2*tau2));
  long double arg3 = (-t/tau3 + sigma*sigma/(2*tau3*tau3));
  long double arg4 = ((sigma*sigma - t*tau1)/(TMath::Sqrt(2)*sigma*tau1));
  long double arg5 = ((sigma*sigma - t*tau2)/(TMath::Sqrt(2)*sigma*tau2));
  long double arg6 = ((sigma*sigma - t*tau3)/(TMath::Sqrt(2)*sigma*tau3));
  long double exp_arg1 = TMath::Exp(arg1);
  long double exp_arg2 = TMath::Exp(arg2);
  long double exp_arg3 = TMath::Exp(arg3);
  long double erfc_arg4 = TMath::Erfc(arg4);
  long double erfc_arg5 = TMath::Erfc(arg5);
  long double erfc_arg6 = TMath::Erfc(arg6);

  return ( amp*((A/tau3)*exp_arg3*erfc_arg6 + ((1.-p-A)/tau2)*exp_arg2*erfc_arg5 + (p/tau1)*exp_arg1*erfc_arg4) );
}

void plotFit() {
  TFile* alphaFile = new TFile("alphaWvfmNoAP.root");
  TGraph* g_alpha = (TGraph*)alphaFile->Get("alphaWvfm1000Evts");
  g_alpha->SetLineColor(kBlue);
  g_alpha->SetLineWidth(2);

  TFile* elecFile = new TFile("elecWvfmNoAP.root");
  TGraph* g_elec = (TGraph*)elecFile->Get("elecWvfm1000Evts");
  g_elec->SetLineColor(kRed);
  g_elec->SetLineWidth(2);

  TCanvas* c1 = new TCanvas();
  c1->cd();
  g_alpha->Draw("ALP");

  TF1* conv_func_3 = new TF1("conv_3comp_exp", conv_3comp_exp, 100, 640, 8);
  conv_func_3->SetParameters(2.1, 190., 0.44, 50, 1, 25, 18.0, 0.16);
  conv_func_3->SetParNames("tau1", "tau2", "f1", "sigma", "amp", "t_0", "tau3", "f3");
  conv_func_3->SetNpx(1000);
  conv_func_3->FixParameter(0, 4.6); //2.1 for alphas
  conv_func_3->FixParameter(1, 156.0); //190.0 for alphas
  conv_func_3->FixParameter(6, 18.0); //18.0 for alphas
  conv_func_3->FixParameter(2, 0.71); //0.44 for alphas
  conv_func_3->FixParameter(7, 0.22); //0.16 for alphas
  conv_func_3->SetLineColor(kViolet);
  //  g_alpha->GetXaxis()->SetRangeUser(0, 700);
  //g_alpha->Fit("conv_3comp_exp", "R");

  
  TCanvas* c2 = new TCanvas();
  c2->cd();
  g_elec->SetTitle("Average waveform of 1000 750 keV electrons");
  g_elec->Draw("ALP");
  //  g_elec->GetXaxis()->SetRangeUser(0, 700);
  //  g_elec->Fit("conv_3comp_exp", "R");

  TCanvas* c3 = new TCanvas();
  g_elec->SetTitle("Average waveform of 1000 events");
  g_elec->Draw("ALP");
  g_alpha->Draw("LP SAME");

  TLegend* leg = new TLegend(0.6, 0.7, 0.85, 0.85);
  leg->AddEntry(g_elec, "750 keV electrons", "l");
  leg->AddEntry(g_alpha, "5.6 MeV alphas", "l");
  leg->Draw("SAME");
  
}
