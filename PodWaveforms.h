//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Feb 25 14:38:26 2021 by ROOT version 6.20/04
// from TTree PodWaveforms/POD waveform data
// found on file: lzap_5MeVAlphas_waveforms.root
//////////////////////////////////////////////////////////

#ifndef PodWaveforms_h
#define PodWaveforms_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TObject.h"

class PodWaveforms {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxeventHeader = 1;
   static constexpr Int_t kMaxcalibratedPods = 1;
   static constexpr Int_t kMaxsummedPodsTPCHG = 1;
   static constexpr Int_t kMaxsummedPodsTPCLG = 1;
   static constexpr Int_t kMaxsummedPodsSkin = 1;
   static constexpr Int_t kMaxsummedPodsODHG = 1;
   static constexpr Int_t kMaxsummedPodsODLG = 1;

   // Declaration of leaf types
 //RQ::EventHeader *eventHeader_;
   UInt_t          eventHeader_TObject_fUniqueID;
   UInt_t          eventHeader_TObject_fBits;
   string          eventHeader_lzapVersion;
   ULong_t         eventHeader_runID;
   ULong_t         eventHeader_eventID;
   string          eventHeader_rawFileName;
   string          eventHeader_rawFileDir;
   ULong_t         eventHeader_triggerTimeStamp_s;
   ULong_t         eventHeader_triggerTimeStamp_ns;
   Int_t           eventHeader_triggerMultiplicity;
   Int_t           eventHeader_triggerType;
   ULong_t         eventHeader_sumPodStartTime_s;
   ULong_t         eventHeader_sumPodStartTime_ns;
   UInt_t          eventHeader_sumPodSampleCount;
   vector<float>   eventHeader_sumPodSamples;
   Int_t           eventHeader_runType;
   ULong_t         eventHeader_runStartTime_s;
   ULong_t         eventHeader_runStartTime_ns;
   Int_t           eventHeader_rawFileFormatVersion;
   vector<int>     eventHeader_nPodsDC;
   Int_t           eventHeader_nPods;
   vector<unsigned long> eventHeader_bufferStartTime_s;
   vector<unsigned long> eventHeader_bufferStartTime_ns;
   vector<unsigned long> eventHeader_bufferStopTime_s;
   vector<unsigned long> eventHeader_bufferStopTime_ns;
 //RQ::Pods        *calibratedPods_;
   UInt_t          calibratedPods_TObject_fUniqueID;
   UInt_t          calibratedPods_TObject_fBits;
   UInt_t          calibratedPods_nPods;
   vector<int>     calibratedPods_channelID;
   vector<int>     calibratedPods_podStartTime_ns;
   vector<vector<float> > calibratedPods_podSamples;
 //RQ::Pods        *summedPodsTPCHG_;
   UInt_t          summedPodsTPCHG_TObject_fUniqueID;
   UInt_t          summedPodsTPCHG_TObject_fBits;
   UInt_t          summedPodsTPCHG_nPods;
   vector<int>     summedPodsTPCHG_channelID;
   vector<int>     summedPodsTPCHG_podStartTime_ns;
   vector<vector<float> > summedPodsTPCHG_podSamples;
 //RQ::Pods        *summedPodsTPCLG_;
   UInt_t          summedPodsTPCLG_TObject_fUniqueID;
   UInt_t          summedPodsTPCLG_TObject_fBits;
   UInt_t          summedPodsTPCLG_nPods;
   vector<int>     summedPodsTPCLG_channelID;
   vector<int>     summedPodsTPCLG_podStartTime_ns;
   vector<vector<float> > summedPodsTPCLG_podSamples;
 //RQ::Pods        *summedPodsSkin_;
   UInt_t          summedPodsSkin_TObject_fUniqueID;
   UInt_t          summedPodsSkin_TObject_fBits;
   UInt_t          summedPodsSkin_nPods;
   vector<int>     summedPodsSkin_channelID;
   vector<int>     summedPodsSkin_podStartTime_ns;
   vector<vector<float> > summedPodsSkin_podSamples;
 //RQ::Pods        *summedPodsODHG_;
   UInt_t          summedPodsODHG_TObject_fUniqueID;
   UInt_t          summedPodsODHG_TObject_fBits;
   UInt_t          summedPodsODHG_nPods;
   vector<int>     summedPodsODHG_channelID;
   vector<int>     summedPodsODHG_podStartTime_ns;
   vector<vector<float> > summedPodsODHG_podSamples;
 //RQ::Pods        *summedPodsODLG_;
   UInt_t          summedPodsODLG_TObject_fUniqueID;
   UInt_t          summedPodsODLG_TObject_fBits;
   UInt_t          summedPodsODLG_nPods;
   vector<int>     summedPodsODLG_channelID;
   vector<int>     summedPodsODLG_podStartTime_ns;
   vector<vector<float> > summedPodsODLG_podSamples;

   // List of branches
   TBranch        *b_eventHeader_TObject_fUniqueID;   //!
   TBranch        *b_eventHeader_TObject_fBits;   //!
   TBranch        *b_eventHeader_lzapVersion;   //!
   TBranch        *b_eventHeader_runID;   //!
   TBranch        *b_eventHeader_eventID;   //!
   TBranch        *b_eventHeader_rawFileName;   //!
   TBranch        *b_eventHeader_rawFileDir;   //!
   TBranch        *b_eventHeader_triggerTimeStamp_s;   //!
   TBranch        *b_eventHeader_triggerTimeStamp_ns;   //!
   TBranch        *b_eventHeader_triggerMultiplicity;   //!
   TBranch        *b_eventHeader_triggerType;   //!
   TBranch        *b_eventHeader_sumPodStartTime_s;   //!
   TBranch        *b_eventHeader_sumPodStartTime_ns;   //!
   TBranch        *b_eventHeader_sumPodSampleCount;   //!
   TBranch        *b_eventHeader_sumPodSamples;   //!
   TBranch        *b_eventHeader_runType;   //!
   TBranch        *b_eventHeader_runStartTime_s;   //!
   TBranch        *b_eventHeader_runStartTime_ns;   //!
   TBranch        *b_eventHeader_rawFileFormatVersion;   //!
   TBranch        *b_eventHeader_nPodsDC;   //!
   TBranch        *b_eventHeader_nPods;   //!
   TBranch        *b_eventHeader_bufferStartTime_s;   //!
   TBranch        *b_eventHeader_bufferStartTime_ns;   //!
   TBranch        *b_eventHeader_bufferStopTime_s;   //!
   TBranch        *b_eventHeader_bufferStopTime_ns;   //!
   TBranch        *b_calibratedPods_TObject_fUniqueID;   //!
   TBranch        *b_calibratedPods_TObject_fBits;   //!
   TBranch        *b_calibratedPods_nPods;   //!
   TBranch        *b_calibratedPods_channelID;   //!
   TBranch        *b_calibratedPods_podStartTime_ns;   //!
   TBranch        *b_calibratedPods_podSamples;   //!
   TBranch        *b_summedPodsTPCHG_TObject_fUniqueID;   //!
   TBranch        *b_summedPodsTPCHG_TObject_fBits;   //!
   TBranch        *b_summedPodsTPCHG_nPods;   //!
   TBranch        *b_summedPodsTPCHG_channelID;   //!
   TBranch        *b_summedPodsTPCHG_podStartTime_ns;   //!
   TBranch        *b_summedPodsTPCHG_podSamples;   //!
   TBranch        *b_summedPodsTPCLG_TObject_fUniqueID;   //!
   TBranch        *b_summedPodsTPCLG_TObject_fBits;   //!
   TBranch        *b_summedPodsTPCLG_nPods;   //!
   TBranch        *b_summedPodsTPCLG_channelID;   //!
   TBranch        *b_summedPodsTPCLG_podStartTime_ns;   //!
   TBranch        *b_summedPodsTPCLG_podSamples;   //!
   TBranch        *b_summedPodsSkin_TObject_fUniqueID;   //!
   TBranch        *b_summedPodsSkin_TObject_fBits;   //!
   TBranch        *b_summedPodsSkin_nPods;   //!
   TBranch        *b_summedPodsSkin_channelID;   //!
   TBranch        *b_summedPodsSkin_podStartTime_ns;   //!
   TBranch        *b_summedPodsSkin_podSamples;   //!
   TBranch        *b_summedPodsODHG_TObject_fUniqueID;   //!
   TBranch        *b_summedPodsODHG_TObject_fBits;   //!
   TBranch        *b_summedPodsODHG_nPods;   //!
   TBranch        *b_summedPodsODHG_channelID;   //!
   TBranch        *b_summedPodsODHG_podStartTime_ns;   //!
   TBranch        *b_summedPodsODHG_podSamples;   //!
   TBranch        *b_summedPodsODLG_TObject_fUniqueID;   //!
   TBranch        *b_summedPodsODLG_TObject_fBits;   //!
   TBranch        *b_summedPodsODLG_nPods;   //!
   TBranch        *b_summedPodsODLG_channelID;   //!
   TBranch        *b_summedPodsODLG_podStartTime_ns;   //!
   TBranch        *b_summedPodsODLG_podSamples;   //!

   PodWaveforms(TTree *tree=0);
   virtual ~PodWaveforms();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef PodWaveforms_cxx
PodWaveforms::PodWaveforms(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("lzap_075keVElec_noAP_waveforms.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("lzap_075keVElec_noAP_waveforms.root");
      }
      f->GetObject("PodWaveforms",tree);

   }
   Init(tree);
}

PodWaveforms::~PodWaveforms()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PodWaveforms::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PodWaveforms::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PodWaveforms::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eventHeader.TObject.fUniqueID", &eventHeader_TObject_fUniqueID, &b_eventHeader_TObject_fUniqueID);
   fChain->SetBranchAddress("eventHeader.TObject.fBits", &eventHeader_TObject_fBits, &b_eventHeader_TObject_fBits);
   fChain->SetBranchAddress("eventHeader.lzapVersion", &eventHeader_lzapVersion, &b_eventHeader_lzapVersion);
   fChain->SetBranchAddress("eventHeader.runID", &eventHeader_runID, &b_eventHeader_runID);
   fChain->SetBranchAddress("eventHeader.eventID", &eventHeader_eventID, &b_eventHeader_eventID);
   fChain->SetBranchAddress("eventHeader.rawFileName", &eventHeader_rawFileName, &b_eventHeader_rawFileName);
   fChain->SetBranchAddress("eventHeader.rawFileDir", &eventHeader_rawFileDir, &b_eventHeader_rawFileDir);
   fChain->SetBranchAddress("eventHeader.triggerTimeStamp_s", &eventHeader_triggerTimeStamp_s, &b_eventHeader_triggerTimeStamp_s);
   fChain->SetBranchAddress("eventHeader.triggerTimeStamp_ns", &eventHeader_triggerTimeStamp_ns, &b_eventHeader_triggerTimeStamp_ns);
   fChain->SetBranchAddress("eventHeader.triggerMultiplicity", &eventHeader_triggerMultiplicity, &b_eventHeader_triggerMultiplicity);
   fChain->SetBranchAddress("eventHeader.triggerType", &eventHeader_triggerType, &b_eventHeader_triggerType);
   fChain->SetBranchAddress("eventHeader.sumPodStartTime_s", &eventHeader_sumPodStartTime_s, &b_eventHeader_sumPodStartTime_s);
   fChain->SetBranchAddress("eventHeader.sumPodStartTime_ns", &eventHeader_sumPodStartTime_ns, &b_eventHeader_sumPodStartTime_ns);
   fChain->SetBranchAddress("eventHeader.sumPodSampleCount", &eventHeader_sumPodSampleCount, &b_eventHeader_sumPodSampleCount);
   fChain->SetBranchAddress("eventHeader.sumPodSamples", &eventHeader_sumPodSamples, &b_eventHeader_sumPodSamples);
   fChain->SetBranchAddress("eventHeader.runType", &eventHeader_runType, &b_eventHeader_runType);
   fChain->SetBranchAddress("eventHeader.runStartTime_s", &eventHeader_runStartTime_s, &b_eventHeader_runStartTime_s);
   fChain->SetBranchAddress("eventHeader.runStartTime_ns", &eventHeader_runStartTime_ns, &b_eventHeader_runStartTime_ns);
   fChain->SetBranchAddress("eventHeader.rawFileFormatVersion", &eventHeader_rawFileFormatVersion, &b_eventHeader_rawFileFormatVersion);
   fChain->SetBranchAddress("eventHeader.nPodsDC", &eventHeader_nPodsDC, &b_eventHeader_nPodsDC);
   fChain->SetBranchAddress("eventHeader.nPods", &eventHeader_nPods, &b_eventHeader_nPods);
   fChain->SetBranchAddress("eventHeader.bufferStartTime_s", &eventHeader_bufferStartTime_s, &b_eventHeader_bufferStartTime_s);
   fChain->SetBranchAddress("eventHeader.bufferStartTime_ns", &eventHeader_bufferStartTime_ns, &b_eventHeader_bufferStartTime_ns);
   fChain->SetBranchAddress("eventHeader.bufferStopTime_s", &eventHeader_bufferStopTime_s, &b_eventHeader_bufferStopTime_s);
   fChain->SetBranchAddress("eventHeader.bufferStopTime_ns", &eventHeader_bufferStopTime_ns, &b_eventHeader_bufferStopTime_ns);
   fChain->SetBranchAddress("calibratedPods.TObject.fUniqueID", &calibratedPods_TObject_fUniqueID, &b_calibratedPods_TObject_fUniqueID);
   fChain->SetBranchAddress("calibratedPods.TObject.fBits", &calibratedPods_TObject_fBits, &b_calibratedPods_TObject_fBits);
   fChain->SetBranchAddress("calibratedPods.nPods", &calibratedPods_nPods, &b_calibratedPods_nPods);
   fChain->SetBranchAddress("calibratedPods.channelID", &calibratedPods_channelID, &b_calibratedPods_channelID);
   fChain->SetBranchAddress("calibratedPods.podStartTime_ns", &calibratedPods_podStartTime_ns, &b_calibratedPods_podStartTime_ns);
   fChain->SetBranchAddress("calibratedPods.podSamples", &calibratedPods_podSamples, &b_calibratedPods_podSamples);
   fChain->SetBranchAddress("summedPodsTPCHG.TObject.fUniqueID", &summedPodsTPCHG_TObject_fUniqueID, &b_summedPodsTPCHG_TObject_fUniqueID);
   fChain->SetBranchAddress("summedPodsTPCHG.TObject.fBits", &summedPodsTPCHG_TObject_fBits, &b_summedPodsTPCHG_TObject_fBits);
   fChain->SetBranchAddress("summedPodsTPCHG.nPods", &summedPodsTPCHG_nPods, &b_summedPodsTPCHG_nPods);
   fChain->SetBranchAddress("summedPodsTPCHG.channelID", &summedPodsTPCHG_channelID, &b_summedPodsTPCHG_channelID);
   fChain->SetBranchAddress("summedPodsTPCHG.podStartTime_ns", &summedPodsTPCHG_podStartTime_ns, &b_summedPodsTPCHG_podStartTime_ns);
   fChain->SetBranchAddress("summedPodsTPCHG.podSamples", &summedPodsTPCHG_podSamples, &b_summedPodsTPCHG_podSamples);
   fChain->SetBranchAddress("summedPodsTPCLG.TObject.fUniqueID", &summedPodsTPCLG_TObject_fUniqueID, &b_summedPodsTPCLG_TObject_fUniqueID);
   fChain->SetBranchAddress("summedPodsTPCLG.TObject.fBits", &summedPodsTPCLG_TObject_fBits, &b_summedPodsTPCLG_TObject_fBits);
   fChain->SetBranchAddress("summedPodsTPCLG.nPods", &summedPodsTPCLG_nPods, &b_summedPodsTPCLG_nPods);
   fChain->SetBranchAddress("summedPodsTPCLG.channelID", &summedPodsTPCLG_channelID, &b_summedPodsTPCLG_channelID);
   fChain->SetBranchAddress("summedPodsTPCLG.podStartTime_ns", &summedPodsTPCLG_podStartTime_ns, &b_summedPodsTPCLG_podStartTime_ns);
   fChain->SetBranchAddress("summedPodsTPCLG.podSamples", &summedPodsTPCLG_podSamples, &b_summedPodsTPCLG_podSamples);
   fChain->SetBranchAddress("summedPodsSkin.TObject.fUniqueID", &summedPodsSkin_TObject_fUniqueID, &b_summedPodsSkin_TObject_fUniqueID);
   fChain->SetBranchAddress("summedPodsSkin.TObject.fBits", &summedPodsSkin_TObject_fBits, &b_summedPodsSkin_TObject_fBits);
   fChain->SetBranchAddress("summedPodsSkin.nPods", &summedPodsSkin_nPods, &b_summedPodsSkin_nPods);
   fChain->SetBranchAddress("summedPodsSkin.channelID", &summedPodsSkin_channelID, &b_summedPodsSkin_channelID);
   fChain->SetBranchAddress("summedPodsSkin.podStartTime_ns", &summedPodsSkin_podStartTime_ns, &b_summedPodsSkin_podStartTime_ns);
   fChain->SetBranchAddress("summedPodsSkin.podSamples", &summedPodsSkin_podSamples, &b_summedPodsSkin_podSamples);
   fChain->SetBranchAddress("summedPodsODHG.TObject.fUniqueID", &summedPodsODHG_TObject_fUniqueID, &b_summedPodsODHG_TObject_fUniqueID);
   fChain->SetBranchAddress("summedPodsODHG.TObject.fBits", &summedPodsODHG_TObject_fBits, &b_summedPodsODHG_TObject_fBits);
   fChain->SetBranchAddress("summedPodsODHG.nPods", &summedPodsODHG_nPods, &b_summedPodsODHG_nPods);
   fChain->SetBranchAddress("summedPodsODHG.channelID", &summedPodsODHG_channelID, &b_summedPodsODHG_channelID);
   fChain->SetBranchAddress("summedPodsODHG.podStartTime_ns", &summedPodsODHG_podStartTime_ns, &b_summedPodsODHG_podStartTime_ns);
   fChain->SetBranchAddress("summedPodsODHG.podSamples", &summedPodsODHG_podSamples, &b_summedPodsODHG_podSamples);
   fChain->SetBranchAddress("summedPodsODLG.TObject.fUniqueID", &summedPodsODLG_TObject_fUniqueID, &b_summedPodsODLG_TObject_fUniqueID);
   fChain->SetBranchAddress("summedPodsODLG.TObject.fBits", &summedPodsODLG_TObject_fBits, &b_summedPodsODLG_TObject_fBits);
   fChain->SetBranchAddress("summedPodsODLG.nPods", &summedPodsODLG_nPods, &b_summedPodsODLG_nPods);
   fChain->SetBranchAddress("summedPodsODLG.channelID", &summedPodsODLG_channelID, &b_summedPodsODLG_channelID);
   fChain->SetBranchAddress("summedPodsODLG.podStartTime_ns", &summedPodsODLG_podStartTime_ns, &b_summedPodsODLG_podStartTime_ns);
   fChain->SetBranchAddress("summedPodsODLG.podSamples", &summedPodsODLG_podSamples, &b_summedPodsODLG_podSamples);
   Notify();
}

Bool_t PodWaveforms::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PodWaveforms::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PodWaveforms::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef PodWaveforms_cxx
